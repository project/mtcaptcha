# MTCaptcha-Drupal7-plugin
Drupal 7 Plugin for MTCaptcha

CONTENTS OF THIS FILE
---------------------
* INTRODUCTION OF MTCAPTCHA
* TOP HIGHLIGHTS OF MTCAPTCHA
* SUPPORTED THEMES
* SUPPORTED LANGUAGES
* SUPPORTED CAPTCHA WIDGET SIZE
* FEATURES PROTECTED BY MTCAPTCHA  DRUPAL MODULE
* REQUIREMENTS
* HOW TO DOWNLOAD MTCAPTCHA MODULE
* INSTALLATION AND CONFIGURATION
* HOW TO GET VALID SITE KEY AND PRIVATE KEY
* FREQUENTLY ASKED QUESTIONS


INTRODUCTION OF MTCAPTCHA
-------------------------
MTCaptcha is an efficient security solution to protect your Drupal website
against spam comments and brute-force attacks. It can be integrated with
the comments, login, registration, forgot password

TOP HIGHLIGHTS OF MTCAPTCHA
---------------------------
* GDPR compliance
* Enterprise friendly
* Accessibility compliance
* Adaptive risk engine
* High availability around the world

SUMMARY OF THE FEATURES
-----------------------
Easy to configure custom skin for captcha which suits your theme.

SUPPORTED THEMES
----------------
* Standard
* Overcast
* Neowhite
* Goldbezel
* Blackmoon
* Darkruby
* Touchoforange
* Caribbean
* Woodyallen
* Chrome
* Highcontrast
You can see the demo of various prebuilt theme options here.
As of now, full customization is not part of the Drupal configuration.

SUPPORTED LANGUAGES
-------------------
MTCaptcha support supports localization for 60+ languages

SUPPORTED CAPTCHA WIDGET SIZE
-----------------------------
* Standard
* Modern Mini
You can choose an awesome mtcaptcha widgetsize to your application forms.

You can see the demo of various localization options here.
As of now custom language is not part of Drupal configuration.

FEATURES PROTECTED BY MTCAPTCHA  DRUPAL MODULE
----------------------------------------------
* Login Form protection
* Registration form protection
* Comments form Protection
* Forgot password form protection
* Contact form protection
* Any other forms you wish to enable MTCaptcha

REQUIREMENTS
------------
This module requires [CAPTCHA](https://www.drupal.org/project/captcha) 
module outside of Drupal core.

HOW TO DOWNLOAD MTCAPTCHA MODULE
--------------------------------
Contributed modules from the Drupal community may be downloaded by the link:
	* https://www.drupal.org/project/mtcaptcha/releases/7.x-dev

INSTALLATION AND CONFIGURATION
------------------------------
1) Navigate to Modules folder in the admin panel
2) Go to Spam Control section and enable the MTCaptcha module
   and hit Save Configuration
3) Once it is enabled, then click on the Permissions button,
now enable at required block section called MTCaptcha and hit Save Permissions
4) Go to Spam Control section and click on the Configure button located
 under Operations column of the MTCaptcha module
5) Configure the Mandatory Fields and hit Save Configuration
	* Mandatory Fields
		- Valid Site key and Private Key is required

HOW TO GET VALID SITE KEY AND PRIVATE KEY
-----------------------------------------
1) Login to admin.mtcaptcha.com
2) Register your domain eg: www.domain.com
3) You will get a "Sitekey" and "Private key" for your domains

FREQUENTLY ASKED QUESTIONS
--------------------------
1Q) How to resolve if the user is receiving Invalid Site Key Error
   instead of Captcha?
A: Ensure that the provided sitekey in configuration page is valid

2Q) How to get SiteKey and Private Key?
A: Login to admin.mtcaptcha.com and register for your domain.
   You will get a "sitekey" and "private key" for your domains.

3Q) My changes are not reflecting in the UI forms based on the configuration?
A: Go to Configuration page then navigate to performance and clear cache
		- Configuration >> Performance and Clear Cache

4Q) More FAQ
A:	[FAQ](https://www.mtcaptcha.com/faq)
