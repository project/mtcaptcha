<?php

/**
 * @file
 * Verifies if user is a human without necessity to solve a CAPTCHA.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_help().
 * @see hook_help()
 */
function mtcaptcha_help($path, $arg) {
  switch ($path) {
      case 'admin/help#mtcaptcha':
      return t('<a href="@url">MTCaptcha</a>  
        is an efficient security solution to protect your website against 
        spam comments and brute-force attacks.',
        array('@url' => 'https://www.mtcaptcha.com/'));
  }
}

/**
 * Implements hook_theme().
 * @see hook_theme()
 */
function mtcaptcha_theme() {
  return array(
    'mtcaptcha_widget_noscript' => array(
      'variables' => array(
        'widget' => NULL,
      ),
      'template' => 'mtcaptcha-widget-noscript',
    ),
  );
}

/**
 * Implements hook_menu().
 * @see hook_menu()
 */
function mtcaptcha_menu() {
  $items['admin/config/mtcaptcha'] = array(
    'title' => t('MTCaptcha'),
    'description' => t('Administer MTCaptcha.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mtcaptcha_admin_settings'),
    'access arguments' => array('administer mtcaptcha'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'mtcaptcha.admin.inc',
    'weight' => 1,
  );

  return $items;
}

/**
 * Implements hook_permission().
 * @see hook_permission()
 */
function mtcaptcha_permission() {
  return array(
    'administer mtcaptcha' => array(
      'title' => t('Administer MTCaptcha'),
      'description' => t('Administer MTCaptcha settings'),
    ),
  );
}

/**
 * Implements hook_captcha().
 * @see hook_captcha()
 */
function mtcaptcha_captcha($op, $captcha_type = '') {
  $user = $GLOBALS['user'];  
  switch ($op) {
    case 'list':
      return array('MTCaptcha');

    case 'generate':
      $captcha = array();
      if ($captcha_type == 'MTCaptcha' &&
         (($user->uid && variable_get('mtcaptcha_enablecaptcha') == 'login') ||
         (!$user->uid && variable_get('mtcaptcha_enablecaptcha') == 'logout') ||
         (variable_get('mtcaptcha_enablecaptcha') == 'all')) ) {
          $mtcaptcha_site_key = variable_get('mtcaptcha_site_key', '');
          $mtcaptcha_private_key = variable_get('mtcaptcha_private_key', '');
          $custom_config_enable = variable_get('custom_config_enable');
             if (((empty($custom_config_enable) && !empty($mtcaptcha_site_key))|| 
                  !empty($custom_config_enable)) && !empty($mtcaptcha_private_key)) {
              $captcha['solution'] = TRUE ;
              $captcha['captcha_validate'] = 'mtcaptcha_captcha_validation' ;
              $captcha['form']['captcha_response'] = array(
                '#type' => 'hidden',
                '#value' => 'no captcha',
              );

              $captcha['cacheable'] = TRUE;

              $mtcaptcha_src = 'https://service.mtcaptcha.com/mtcv1/client/mtcaptcha.min.js';
              $mtcaptcha_src_2 = 'https://service.mtcaptcha.com/mtcv1/client/mtcaptcha2.min.js';
              $GLOBALS['configIdList'] = isset($GLOBALS['configIdList']) ? $GLOBALS['configIdList'] : [];
              if($custom_config_enable) {            
                $attributes = array(
                  '#tag' => 'script',
                  '#attributes' => array('type' => 'text/javascript'),
                  '#value' => variable_get('custom_config_setting')
                );
                drupal_add_html_head($attributes, 'script');
              } else {
                $script = array(
                  '#tag' => 'script',
                  '#attributes' => array('type' => 'text/javascript'),
                  '#value' => 'var mtcaptchaConfig ='
                  .json_encode(array(
                    'sitekey' => variable_get('mtcaptcha_site_key', ''),
                    'theme' => variable_get('mtcaptcha_theme', ''),
                    "autoFormValidate"  => (bool)true,
                    "render"      => "explicit",
                    "renderQueue"       => $GLOBALS['configIdList'],
                    "widgetSize"        => variable_get('mtcaptcha_widgetsize', ''),
                    'lang' => variable_get('mtcaptcha_language', '')
                    )),
                 );
                $data = array(
                  '#tag' => 'script',
                  '#value' => '',
                  '#attributes' => array(
                    'src' => url($mtcaptcha_src, array('absolute' => TRUE)),
                    'async' => 'async',
                    'defer' => 'defer',
                  ),
                );
                $data2 = array(
                  '#tag' => 'script',
                  '#value' => '',
                  '#attributes' => array(
                    'src' => url($mtcaptcha_src_2, array('absolute' => TRUE)),
                    'async' => 'async',
                    'defer' => 'defer',
                  ),
                );
                drupal_add_html_head($data, 'mtcpatcha_api');
                drupal_add_html_head($data2, 'mtcpatcha_api_2');
                drupal_add_html_head($script, 'mtcaptcha_config');
              }
              $randomId = "mtcaptcha-" . strval(rand());    
              pushTorenderQueue($randomId);
              $markup = '<div id="' . $randomId . '"></div>';
              $captcha['form']['mtcaptcha_widget'] = array(
                  '#markup' => $markup
              );
            }
        }
      return $captcha;
  }
}

 /**
 * Method to push generated random id in renderedQueue
 * @param $randomId
 */
function pushTorenderQueue($randomId) {
  $GLOBALS['configIdList'] = isset($GLOBALS['configIdList']) ? $GLOBALS['configIdList'] : [];  
  array_push($GLOBALS['configIdList'], $randomId);

  $script = array(
    '#tag' => 'script',
    '#attributes' => array('type' => 'text/javascript'),
    '#value' => 'setTimeout(function(){
      if (typeof mtcaptchaConfig != "undefined") {
        // push the random id in render queue
        mtcaptchaConfig.renderQueue.push("' . $randomId . '");
      }
    })',
    '#weight' => 1000,
  );
  drupal_add_html_head($script, 'mtcaptcha_add_render_queue_' . $randomId);
}

/**
* Implements hook_form_alter().
* This function adds a validation for mtcaptcha admin settings
*/
function mtcaptcha_form_alter(&$form, &$form_state, $form_id) {  
  if($form_id == 'mtcaptcha_admin_settings') {
    $form['#validate'][] = 'mtcaptcha_captcha_admin_validation';
    return false;
   }  
}

/**
  * Method to validate captcha while form submission
  * @param form
  * @param form_state
 */ 
function mtcaptcha_captcha_admin_validation(&$form,  &$form_state) {
  $custom_config_enable  = $form_state['values']['custom_config_enable'];
  $mtcaptcha_site_key  = $form_state['values']['mtcaptcha_site_key'];
  $mtcaptcha_custom_config_value  = $form_state['values']['custom_config_setting'];
  if(!$custom_config_enable && trim($mtcaptcha_site_key) == "") {
    $form['general']['mtcaptcha_site_key']['#required'] = TRUE;
    $form['advanced']['custom_config_setting']['#required'] = FALSE;
    $form['general']['mtcaptcha_site_key']['#attributes'] = array(
      'class' => array('error'), 
    );   
    form_set_error('mtcaptcha', t('Site key field is required'));
    return false;
  } else if($custom_config_enable && trim($mtcaptcha_custom_config_value) == "" ) {
    $form['general']['mtcaptcha_site_key']['#required'] = FALSE;
    $form['advanced']['custom_config_setting']['#required'] = TRUE;
    $form['advanced']['custom_config_setting']['#attributes'] = array(
      'class' => array('error'), 
    );
    form_set_error('mtcaptcha', 
      'Custom MTCaptcha Configuration field is required');
    return false;
  }
  return true;
}

/**
 * MTCAPTCHA Callback; Validates the MTCaptcha code.
 */
function mtcaptcha_captcha_validation($solution, 
  $response, $element, $form_state) {
  $mtcaptcha_private_key = variable_get('mtcaptcha_private_key', '');
  if(empty($_POST['mtcaptcha-verifiedtoken']) || empty($mtcaptcha_private_key)) {
    form_set_error('mtcaptcha', 
      t('The token/private key paramter is invalid or malformed'));
    watchdog('MTCaptcha web service', 
      '@error', 
       array('@error' => 'The token/private key paramter is invalid or malformed'), 
       WATCHDOG_ERROR);
    return FALSE;
  }
  $resp = mtcaptcha_submit($mtcaptcha_private_key, 
    t($_POST['mtcaptcha-verifiedtoken']));

  $resp = json_decode($resp, 1);
  if ($resp["success"]) {    
    return TRUE;
  } else {
    $error_codes = array(
      'token-expired' => t('The token has expired.'),
      'token-duplicate-cal' => t('The token has been verified already.'),
      'bad-request' => t('The request 
        is invalid or malformed.'),
      'missing-input-privatekey' => t('`privatekey` parameter is missing'),
      'missing-input-token' => t(' ‘token’ parameter is missing.'),
      'invalid-privatekey' => t('The private 
        key parameter is invalid or malformed.'),
      'invalid-token' => t('The token parameter is invalid or malformed.'),
      'invalid-token-faildecrypt' => t('The token 
        parameter is invalid or malformed.'),
      'privatekey-mismatch-token' => t('The token and 
        the privatekey does not match.'),
      'expired-sitekey-or-account' => t('The sitekey/privatekey is 
        no longer valid due to expiration or account closure.'),
      'network-error' => t('Something went wrong!'),
    );
    foreach ($resp["fail_codes"] as $code) {
      if (!isset($error_codes[$code])) {
        $code = 'network-error';
      }
      form_set_error('mtcaptcha', $error_codes[$code]);
      watchdog('MTCaptcha web service', 
        '@error', 
         array('@error' => $error_codes[$code]), 
         WATCHDOG_ERROR);
    }
  }
  return FALSE;
}

/**
 * Validates and submits the MTCaptcha code.
 */
function mtcaptcha_submit($mt_captcha_site_private_key, 
  $mtcaptcha_verifiedtoken) {

  $options = array(
    'method' => 'GET'
  );

  $response = drupal_http_request("https://service.mtcaptcha.com/mtcv1/api/checktoken?privatekey={$mt_captcha_site_private_key}&token={$mtcaptcha_verifiedtoken}", $options);
  if ($response->code == 200 && isset($response->data)) {
    // The service request was successful.
    return $response->data;
  }
  elseif ($response->code < 0) {
    // Negative status codes typically point to network or socket issues.
    return '{"success": false, "error-codes": ["network-error"]}';
  }
  else {
    // Positive none 200 status code typically means the request has failed.
    return $response->data;
  }
}

